echo "BUILD START...."

# Create and activate a virtual environment
python3.9 -m venv env
source env/bin/activate

echo "INSTALLING REQUIREMENTS"
python3.9 -m pip install --upgrade pip
python3.9 -m pip install -r requirements.txt

echo "BUILDING MIGRATIONS"
python3.9 manage.py makemigrations --noinput

echo "APPLYING MIGRATIONS"
python3.9 manage.py migrate --noinput

echo "BUILDING STATIC FILES"
python3.9 manage.py collectstatic --noinput --clear

echo "BUILD FINISHED"
