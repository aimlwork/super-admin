from django.shortcuts import render

# Create your views here.


def base(request):
    return render(request, 'base.html')

def about(request):
    return render(request, 'admin_app/about.html')

def plans(request):
    return render(request, 'admin_app/plans.html')