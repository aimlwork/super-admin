from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import User
from django import forms
from .models import *
from django.forms.widgets import DateInput, DateTimeInput

class CalendarDateInput(DateInput):
    input_type = 'date'

    def __init__(self, attrs=None, format=None):
        attrs = {'class': 'datepicker'}  # You can add more attributes as needed
        super().__init__(attrs, format)

class CalendarDateTimeInput(DateTimeInput):
    input_type = 'datetime-local'

    def __init__(self, attrs=None, format=None):
        attrs = {'class': 'datetimepicker'}  # You can add more attributes as needed
        super().__init__(attrs, format)




class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ('username', 'password',)


from .models import Appointment

class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['date', 'lead_id', 'status', 'remarks', 'phone']



from . models import TaskManager

class TaskManagerForm(forms.ModelForm):

    class Meta:
        model = TaskManager
        fields = '__all__'
        widgets = {
            'date': CalendarDateInput(),
        }




from .models import SalesTracker

class SalesTrackerForm(forms.ModelForm):
    class Meta:
        model = SalesTracker
        fields = ['date', 'lead_id', 'status', 'remarks', 'phone']


class LeadForm(forms.ModelForm):
    class Meta:
        model = Lead
        fields = '__all__'
        widgets = {           # Added the calander feature
            'date_of_birth': CalendarDateInput(),
            'Available_date': CalendarDateInput(),

        }



# HRM/attendence


from .models import Attendence

class AttendenceForm(forms.ModelForm):
    class Meta:
        model = Attendence
        fields = '__all__'
        widget = {   
            'checkin_time': CalendarDateTimeInput(),
            'checkout_time': CalendarDateTimeInput(),
            'created_date': CalendarDateInput(),

        }


# HRM/notice


from .models import Notice

class NoticeForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = '__all__'
        widgets = {
            'notice_date': CalendarDateInput(),
        }

    
from . models import Payroll

class PayrollForm(forms.ModelForm):
    class Meta:
        model = Payroll
        fields = '__all__'


# HRM/performance

from .models import Performance

class PerformanceForm(forms.ModelForm):
    class Meta:
        model = Performance
        fields = '__all__'



# settings/ general_settings

from .models import GeneralSetting

class GeneralSettingForm(forms.ModelForm):
    class Meta:
        model = GeneralSetting
        fields = '__all__'
        

# settings/ currency_settings

from .models import CurrencySetting

class CurrencySettingForm(forms.ModelForm):
    class Meta:
        model = CurrencySetting
        fields = '__all__'


# settings/ tax_settings

from .models import TaxSetting

class TaxSettingForm(forms.ModelForm):
    class Meta:
        model = TaxSetting
        fields = '__all__'


# settings/ sms_setting

from .models import SmsSetting

class SmsSettingForm(forms.ModelForm):
    class Meta:
        model = SmsSetting
        fields = '__all__'



# settings/ email_setting

from .models import EmailSetting

class EmailSettingForm(forms.ModelForm):
    class Meta:
        model = EmailSetting
        fields = '__all__'

# setting/ location_setting

from .models import LocationSetting 

class LocationSettingForm(forms.ModelForm):
    class Meta:
        model = LocationSetting
        fields = '__all__'

# setting / lead-status_setting

from .models import LeadStatusSetting

class LeadStatusSettingForm(forms.ModelForm):
    class Meta:
        model = LeadStatusSetting
        fields = '__all__'


from .models import LeadSourceSetting

class LeadSourceSettingForm(forms.ModelForm):
    class Meta:
        model = LeadSourceSetting
        fields = '__all__'


from .models import CallStatusSetting

class CallStatusSettingForm(forms.ModelForm):
    class Meta:
        model = CallStatusSetting
        fields = '__all__'


from .models import NotificationSetting

class NotificationSettingForm(forms.ModelForm):
    class Meta:
        model = NotificationSetting
        fields = '__all__'


from .models import CustomFieldSetting

class CustomFieldSettingForm(forms.ModelForm):
    class Meta:
        model = CustomFieldSetting
        fields = '__all__'


from .models import UserManagement

class UserManagementForm(forms.ModelForm):
    class Meta:
        model = UserManagement
        fields = '__all__'