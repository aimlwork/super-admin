from django.contrib.auth.models import AbstractUser
from django.db import models
from django.forms import DateInput



class User(AbstractUser):
    pass


class Appointment(models.Model):
    date = models.DateField()
    lead_id = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    remarks = models.TextField()
    phone = models.CharField(max_length=20)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Appointment for {self.date} (Lead ID: {self.lead_id})"
    


class SalesTracker(models.Model):
    date = models.DateField()
    lead_id = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    remarks = models.TextField()
    phone = models.CharField(max_length=20)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"SalesTracker for {self.date} (Lead ID: {self.lead_id})"


class TaskManager(models.Model):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=20)
    date = models.DateField()
    Project_Name = models.CharField(max_length=100)
    Project_Details = models.TextField()
    Project_Type = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    assigned_to = models.CharField(max_length=100)
    lead_circle = models.CharField(max_length=100)
    source = models.CharField(max_length=100)
    lead_id = models.CharField(max_length=100)

    def __str__(self):
        return f"TaskManager for {self.date} (Lead ID: {self.lead_id})"




class Lead(models.Model):
    Name = models.CharField(max_length=50)
    Email = models.EmailField()
    Mobile = models.CharField(max_length=20)
    date_of_birth = models.DateField()
    Available_date = models.DateField()
    configuration = models.CharField(max_length=100)
    Project_Name = models.CharField(max_length=100)
    Project_Details = models.TextField()
    Project_Type = models.CharField(max_length=100)
    Address = models.TextField()
    Country = models.CharField(max_length=100)
    State = models.CharField(max_length=100)
    City = models.CharField(max_length=100)
    Lead_Status = models.CharField(max_length=100)    
    Lead_source = models.CharField(max_length=100)
    Assigned_to = models.CharField(max_length=100)
    Lead_value = models.CharField(max_length=100)
    lead_id = models.CharField(max_length=100)

    def __str__(self):
        return f"Lead for {self.Available_date} (Lead ID: {self.lead_id})"
    


# HRM/attendence

class Attendence(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    checkin_time = models.DateTimeField()
    checkout_time = models.DateTimeField()
    status = models.CharField(max_length=100)
    created_date = models.DateField()

    def __str__(self):
        return f"Attendence for {self.name}"
    

#HRM/notice

class Notice(models.Model):
    id = models.AutoField(primary_key=True)
    notice_type = models.CharField(max_length=100)
    description = models.TextField()
    notice_date = models.DateField()
    notice_by = models.CharField(max_length=100)

    def __str__(self):
        return f"Notice for {self.notice_type}"
    

#HRM/payroll

class Payroll(models.Model):
    employee_id = models.AutoField(primary_key=True)
    employee_name = models.CharField(max_length=100)
    net_salary = models.CharField(max_length=100)
    basic_salary = models.CharField(max_length=100)
    TDS = models.CharField(max_length=100)
    DA = models.CharField(max_length=100)
    HRA = models.CharField(max_length=100)
    PF = models.CharField(max_length=100)
    other1 = models.CharField(max_length=100)
    other2 = models.CharField(max_length=100)

    def __str__(self):
        return f"Payroll for {self.employee_name}"


# HRM/performance

class Performance(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    performance = models.CharField(max_length=100)

    def __str__(self):
        return f"Performance for {self.name}"
    

# settings/general settings ---> 

class GeneralSetting(models.Model):
    project_name = models.CharField(max_length=100)
    project_owner = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    website_url = models.URLField()
    

    def __str__(self):
        return f"GeneralSetting for {self.name}"
    

# settings/currency settings ---> 

class CurrencySetting(models.Model):
    default_country = models.CharField(max_length=100)
    currency_code = models.CharField(max_length=3)
    currency_symbol = models.CharField(max_length=3)
    language = models.CharField(max_length=100)
    time_zone = models.CharField(max_length=100)
    date_format = models.DateField(("dd/mm/yyyy"), auto_now=False, auto_now_add=False)

    def __str__(self):
        return f"CurrencySetting for {self.currency_code}"
    

# settings/email settings --->

class EmailSetting(models.Model):
    smtp_host = models.CharField(max_length=100)
    mail_drom_name = models.CharField(max_length=100)
    mail_from_email_address = models.EmailField()
    mail_host = models.CharField(max_length=100)
    mail_port = models.CharField(max_length=100)
    mail_username= models.CharField(max_length=100)
    mail_password = models.CharField(max_length=100)
    mail_security = models.CharField(max_length=100)

    def __str__(self):
        return f"EmailSetting for {self.mail_from_email_address}"
    
# setting /sms settings --->

class SmsSetting(models.Model):
    sms_gateway = models.SET_DEFAULT = "Twillio"
    account_SID = models.CharField(max_length=100)
    auth_token = models.CharField(max_length=100)
    phone_number =models.CharField(max_length=10)

    def __str__(self):
        return f"SmsSetting for {self.phone_number}"
    

# settings / tax setting -->

class TaxSetting(models.Model):
    tax_name = models.CharField(max_length=100)
    Rate = models.CharField(max_length=100)

    def __str__(self):
        return f"TaxSetting for {self.tax_name}"
    

# settings / location setting -->

class LocationSetting(models.Model):
    location = models.CharField(max_length=100)

    def __str__(self):
        return f"LocationSetting for {self.location}"
    
# settings / Lead Status Settings -->

class LeadStatusSetting(models.Model):
    lead_status = models.CharField(max_length=100)

    def __str__(self):
        return f"LeadStatusSetting for {self.lead_status}"
    

# settings / Lead sourceSettings -->

class LeadSourceSetting(models.Model):
    lead_source = models.CharField(max_length=100)

    def __str__(self):
        return f"LeadSourceSetting for {self.lead_source}"
    

# settings / call_status_Settings -->

class CallStatusSetting(models.Model):
    call_status = models.CharField(max_length=100)  

    def __str__(self):
        return f"CallStatusSetting for {self.call_status}"
    

# settings / custom_field_Settings -->

class CustomFieldSetting(models.Model):
    module = models.CharField(max_length=100)
    label = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)

    def __str__(self):
        return f"CustomFieldSetting for {self.name}"


# setting / notification_settings -->

class NotificationSetting(models.Model):
    firebase_project_id = models.CharField(max_length=100)
    firebase_project_number = models.CharField(max_length=100)
    firebase_app_id = models.CharField(max_length=100)
    firebase_server_key = models.CharField(max_length=100)
    firebase_sender_id = models.CharField(max_length=100)

    def __str__(self):
        return f"NotificationSetting for {self.firebase_project_id}"

    
# user_management -->


class Role(models.Model):

    name = models.CharField(max_length=100, null=False)

    def __str__(self):
        return f"Role for {self.name}"
    

class UserManagement(models.Model):
    @property
    def employee_id(self):
        return f'EMP{self.id:03d}'
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.IntegerField( null=False, blank=False)
    password = models.CharField(max_length=100)
    date_of_birth = models.DateField(("dd/mm/yyyy"), auto_now=False, auto_now_add=False)
    GENDER_CHOICES = [
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other'),
    ]
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    image = models.ImageField(upload_to="media\img", null=True, blank=True)
    address = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    pincode =models.IntegerField(null=False, blank=False)
    bank_name = models.CharField(max_length=100)
    account_number =models.IntegerField(null=False, blank=False)
    ifsc_code = models.CharField(max_length=100)
    account_holder_name = models.CharField(max_length=100)
    upi_id = models.CharField(max_length=100)
    aadhar_card_number = models.IntegerField(null=False, blank=False)
    aadhar_card_image = models.ImageField(upload_to="media\img", null=True, blank=True)
    pan_card_number = models.CharField(max_length=100, null=False, blank=False)
    pan_card_image = models.ImageField(upload_to="media\img", null=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.employee_id}"



