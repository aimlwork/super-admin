from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import SignupForm, LoginForm
from django.views.decorators.csrf import csrf_exempt
from .forms import *
from . models import *
from django.contrib import messages



@csrf_exempt
def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        print(form)
        if form.is_valid():
            user = form.save()
            print(user)
            authenticate(username=user.username, password=user.password)

            if user is not None:
                login(request, user)

                return redirect('/')
        else:
            messages.error(request, 'Invalid username or password')
    else:
        form = SignupForm()

    return render(request, 'accounts_app/user_signup.html', {
        'form': form
    })


def user_login(request):
    if request.method == 'POST':
        print(request.POST)
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('accounts_app:dashboard')
        else:
            messages.error(request, 'Invalid username or password')
    
    form = LoginForm()
    return render(request, 'accounts_app/user_login.html', {'form': form})


@login_required
def user_logout(request):
    logout(request)
    return redirect('/')

@login_required
def dashboard(request):
    return render(request, 'accounts_app/dashboard/dashboard.html')

@login_required
def performance(request):
    performance = Performance.objects.all()
    return render(request, 'accounts_app/HRM/performance.html', {'performance': performance})

@login_required
def add_performance(request):
    if request.method == 'POST':
        form = PerformanceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:performance')
    else:
        form = PerformanceForm()
    return render(request, 'accounts_app/HRM/add_performance.html', {'form': form})

@login_required
def update_performance(request, pk):
    if request.method == 'POST':
        performance = get_object_or_404(Performance, pk=pk)
        form = PerformanceForm(request.POST, instance=performance)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:performance')
    else:
        performance = get_object_or_404(Performance, pk=pk)
        form = PerformanceForm(instance=performance)
    return render(request, 'accounts_app/HRM/update_performance.html', {'form': form})

@login_required
def delete_performance(request, pk):
    if request.method == 'POST':
        performance = get_object_or_404(Performance, pk=pk)
        performance.delete()
        return redirect('accounts_app:performance')
    return render(request, 'accounts_app/HRM/delete_performance.html', {'performance': performance})

@login_required
def payroll(request):
    payroll = Payroll.objects.all()
    return render(request, 'accounts_app/HRM/payroll.html', {'payroll': payroll})

@login_required
def add_payroll(request):
    if request.method == 'POST':
        form = PayrollForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:payroll')
    else:
        form = PayrollForm()
    return render(request, 'accounts_app/HRM/add_payroll.html', {'form': form})

@login_required
def update_payroll(request, pk):
    payroll = get_object_or_404(Payroll, pk=pk)
    if request.method == 'POST':
        form = PayrollForm(request.POST, instance=payroll)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:payroll')
    else:
        form = PayrollForm(instance=payroll)
    return render(request, 'accounts_app/HRM/update_payroll.html', {'form': form})

@login_required
def delete_payroll(request, pk):
    if request.method == 'POST':
        payroll = get_object_or_404(Payroll, pk=pk)
        payroll.delete()
        return redirect('accounts_app:payroll')
    return render(request, 'accounts_app/HRM/payroll.html', {'payroll': payroll})


@login_required
def notice(request):
    notice = Notice.objects.all()
    return render(request, 'accounts_app/HRM/notice.html', {'notice': notice})


@login_required
def add_notice(request):
    if request.method == 'POST':
        form = NoticeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:notice')
    else:
        form = NoticeForm()
    return render(request, 'accounts_app/HRM/add_notice.html', {'form': form})

@login_required
def update_notice(request, pk):
    notice = get_object_or_404(Notice, pk=pk)
    if request.method == 'POST':
        form = NoticeForm(request.POST, instance=notice)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:notice')
    else:
        form = NoticeForm(instance=notice)
    return render(request, 'accounts_app/HRM/update_notice.html', {'form': form})


@login_required
def delete_notice(request, pk):
    notice = get_object_or_404(Notice, pk=pk)
    if request.method == 'POST':
        notice.delete()
        return redirect('accounts_app:notice')
    return render(request, 'accounts_app/HRM/delete_notice.html', {'notice': notice})




@login_required
def attendence(request):
    attendence = Attendence.objects.all()
    return render(request, 'accounts_app/HRM/attendence.html', {'attendence': attendence})

@login_required
def add_attendence(request):
    if request.method == 'POST':
        form = AttendenceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:attendence')
    else:
        form = AttendenceForm()
    return render(request, 'accounts_app/HRM/add_attendence.html', {'form': form})

@login_required
def update_attendence(request, pk):
    attendence = get_object_or_404(Attendence, pk=pk)
    if request.method == 'POST':
        form = AttendenceForm(request.POST, instance=attendence)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:attendence')
    else:
        form = AttendenceForm(instance=attendence)
    return render(request, 'accounts_app/HRM/update_attendence.html', {'form': form})





@login_required
def delete_attendence(request, pk):
    attendence = get_object_or_404(Attendence, pk=pk)
    if request.method == 'POST':
        attendence.delete()
        return redirect('accounts_app:attendence')
    return render(request, 'accounts_app/HRM/delete_attendence.html', {'attendence': attendence})



@login_required
def addadjustement(request):
    return render(request, 'accounts_app/adjustments/addadjustement.html')

@login_required
def adjustmentlist(request):
    return render(request, 'accounts_app/adjustments/adjustmentlist.html')

@login_required
def brands(request):
    return render(request, 'accounts_app/brands.html')

@login_required
def products(request):
    return render(request, 'accounts_app/products.html')

@login_required
def categories(request):
    return render(request, 'accounts_app/categories.html')

@login_required
def sub_categories(request):
    return render(request, 'accounts_app/sub-categories.html')

@login_required
def biller(request):
    return render(request, 'accounts_app/biller.html')

@login_required
def addsuppliers(request):
    return render(request, 'accounts_app/addsuppliers.html')

@login_required
def suppliers(request):
    return render(request, 'accounts_app/suppliers.html')

@login_required
def saleslist(request):
    return render(request, 'accounts_app/saleslist.html')

@login_required
def addsales(request):
    return render(request, 'accounts_app/addsales.html')

@login_required
def addpurchase(request):
    return render(request, 'accounts_app/addpurchase.html')

@login_required
def purchaselist(request):
    return render(request, 'accounts_app/purchaselist.html')

@login_required
def telecalling(request):
    return render(request, 'accounts_app/telecalling.html')

@login_required
def conversations(request):
    return render(request, 'accounts_app/conversations.html')

@login_required
def appointments(request):
    appointments = Appointment.objects.all()  
    return render(request, 'accounts_app/appointments/appointments.html', {'appointments': appointments})

@login_required
def create_appointment(request):
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:appointments')
    else:
        form = AppointmentForm()
    return render(request, 'accounts_app/appointments/create_appointment.html', {'form': form})


@login_required
def update_appointment(request, pk):
    appointment = get_object_or_404(Appointment, pk=pk)
    if request.method == 'POST':
        form = AppointmentForm(request.POST, instance=appointment)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:appointments')
    else:
        form = AppointmentForm(instance=appointment)
    return render(request, 'accounts_app/appointments/update_appointment.html', {'form': form})


@login_required
def delete_appointment(request, pk):
    appointment = get_object_or_404(Appointment, pk=pk)
    if request.method == 'POST':
        appointment.delete()
        return redirect('accounts_app:appointments')
    return render(request, 'accounts_app/appointments/delete_appointment.html', {'appointment': appointment})


@login_required
def salestracker(request):
    salestracker = SalesTracker.objects.all()
    return render(request, 'accounts_app/salestracker/salestracker.html', {'salestracker': salestracker})

@login_required
def create_salestracker(request):
    if request.method == 'POST':
        form = SalesTrackerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:salestracker')
    else:
        form = SalesTrackerForm()
    return render(request, 'accounts_app/salestracker/create_salestracker.html', {'form': form})

@login_required
def update_salestracker(request, pk):
    salestracker = get_object_or_404(SalesTracker, pk=pk)
    if request.method == 'POST':
        form = SalesTrackerForm(request.POST, instance=salestracker)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:salestracker')
    else:
        form = SalesTrackerForm(instance=salestracker)
    return render(request, 'accounts_app/salestracker/update_salestracker.html', {'form': form})

@login_required
def delete_salestracker(request, pk):
    salestracker = get_object_or_404(SalesTracker, pk=pk)
    if request.method == 'POST':
        salestracker.delete()
        return redirect('accounts_app:salestracker')
    return render(request, 'accounts_app/salestracker/delete_salestracker.html', {'salestracker': salestracker})

@login_required
def leads(request):
    lead= Lead.objects.all()
    return render(request, 'accounts_app/lead/leads.html', {'lead': lead})

@login_required
def add_lead(request):
    if request.method == 'POST':
        form = LeadForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:leads')
    else:
        form = LeadForm()
    return render(request, 'accounts_app/lead/add_lead.html', {'form': form})


@login_required
def update_lead(request, pk):
    lead = get_object_or_404(Lead, pk=pk)
    if request.method == 'POST':
        form = LeadForm(request.POST, instance=lead)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:leads')
    else:
        form = LeadForm(instance=lead)
    return render(request, 'accounts_app/lead/update_lead.html', {'form': form})

@login_required
def delete_lead(request, pk):
    lead = get_object_or_404(Lead, pk=pk)
    if request.method == 'POST':
        lead.delete()
        return redirect('accounts_app:leads')
    return render(request, 'accounts_app/lead/delete_lead.html', {'lead': lead})

@login_required
def taskmanager(request):
    taskmanager = TaskManager.objects.all()
    return render(request, 'accounts_app/taskmanager/taskmanager.html', {'taskmanager': taskmanager})

@login_required
def add_taskmanager(request):
    if request.method == 'POST':
        form = TaskManagerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:taskmanager')
    else:
        form = TaskManagerForm()
    return render(request, 'accounts_app/taskmanager/add_taskmanager.html', {'form': form})

@login_required
def update_taskmanager(request, pk):
    if request.method == 'POST':
        form = TaskManagerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:taskmanager')
    else:
        form = TaskManagerForm()
    return render(request, 'accounts_app/taskmanager/update_taskmanager.html', {'form': form})

@login_required
def delete_taskmanager(request, pk):
    if request.method == 'POST':
        taskmanager = get_object_or_404(TaskManager, pk=pk)
        taskmanager.delete()
        return redirect('accounts_app:taskmanager')
    return render(request, 'accounts_app/taskmanager/delete_taskmanager.html', {'taskmanager': taskmanager})



@login_required
def employees(request):
    usermanagement = UserManagement.objects.all()
    return render(request, 'accounts_app/user_management/employees.html', {'usermanagement': usermanagement})

@login_required
def add_employees(request):
    if request.method == 'POST':
        form = UserManagementForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:taskmanager')
    else:
        form = UserManagementForm()
    return render(request, 'accounts_app/user_management/add_employees.html', {'form': form})

@login_required
def update_employees(request, pk):
    if request.method == 'POST':
        form = UserManagementForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:employees')
    else:
        form = UserManagementForm()
    return render(request, 'accounts_app/user_management/update_employees.html', {'form': form})


@login_required
def delete_employees(request, pk):
    if request.method == 'POST':
        taskmanager = get_object_or_404(TaskManager, pk=pk)
        taskmanager.delete()
        return redirect('accounts_app:taskmanager')
    return render(request, 'accounts_app/user_management/delete_employees.html', {'taskmanager': taskmanager})


@login_required
def properties(request):
    return render(request, 'accounts_app/properties.html')

@login_required
def propertymanager(request):
    return render(request, 'accounts_app/propertymanager.html')

@login_required
def customers(request):
    return render(request, 'accounts_app/customers.html')

@login_required
def calendar(request):
    return render(request, 'accounts_app/calendar.html')


@login_required
def addbiller(request):
    return render(request, 'accounts_app/addbiller.html')


@login_required
def general_setting(request):
    if request.method == 'POST':
        form = GeneralSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:general_setting')
    else:
        form = GeneralSettingForm()
    return render(request, 'accounts_app/settings/general_setting.html', {'form': form})


@login_required
def currency_setting(request):
    if request.method == 'POST':
        form = CurrencySettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:currency_setting')
    else:
        form = CurrencySettingForm()
    return render(request, 'accounts_app/settings/currency_setting.html', {'form': form})


@login_required
def email_setting(request):
    if request.method == 'POST':
        form = EmailSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:email_setting')
    else:
        form = EmailSettingForm()
    return render(request, 'accounts_app/settings/email_setting.html', {'form': form})


@login_required
def sms_setting(request):
    if request.method == 'POST':
        form = SmsSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:sms_setting')
    else:
        form = SmsSettingForm()
    return render(request, 'accounts_app/settings/sms_setting.html', {'form': form})


@login_required
def tax_setting(request):
    if request.method == 'POST':
        form = TaxSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:tax_setting')
    else:
        form = TaxSettingForm()
    return render(request, 'accounts_app/settings/tax_setting.html', {'form': form})


@login_required
def location_setting(request):
    if request.method=='POST':
        form = LocationSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:location_setting')
    else:
            form = LocationSettingForm()
    return render(request, 'accounts_app/settings/location_setting.html', {'form':form})
    
@login_required
def lead_status_setting(request):
    if request.method=='POST':
        form = LeadStatusSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:lead_status_setting')
    else:
            form = LeadStatusSettingForm()
    return render(request, 'accounts_app/settings/lead_status_setting.html', {'form':form})
    

@login_required
def lead_source_setting(request):
    if request.method=='POST':
        form = LeadSourceSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:lead_source_setting')
    else:
            form = LeadSourceSettingForm()
    return render(request, 'accounts_app/settings/lead_source_setting.html', {'form':form})
    

@login_required
def call_status_setting(request):
    if request.method=='POST':
        form = CallStatusSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:call_status_setting')
    else:
            form = CallStatusSettingForm()
    return render(request, 'accounts_app/settings/call_status_setting.html', {'form':form})
    

@login_required
def notification_setting(request):
    if request.method=='POST':
        form = NotificationSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:notification_setting')
    else:
            form = NotificationSettingForm()
    return render(request, 'accounts_app/settings/notification_setting.html', {'form':form})
    

@login_required
def custom_field_setting(request):
    if request.method=='POST':
        form = CustomFieldSettingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts_app:custom_field_setting')
    else:
            form = CustomFieldSettingForm()
    return render(request, 'accounts_app/settings/custom_field_setting.html', {'form':form})