from django.contrib import admin

# Register your models here.
from . models import User

AdminUser = admin.site.register(User)



from . models import Appointment

class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('date', 'lead_id', 'status', 'remarks', 'phone', 'last_updated')
    search_fields = ['date', 'lead_id', 'status', 'remarks', 'phone']
    list_filter = ('date', 'status', 'last_updated')

admin.site.register(Appointment, AppointmentAdmin)



from . models import TaskManager

class TaskManagerAdmin(admin.ModelAdmin):
    list_display = ('date', 'lead_id', 'status','mobile')
    search_fields = ['date', 'lead_id', 'status','phone']
    list_filter = ('date', 'status')

admin.site.register(TaskManager, TaskManagerAdmin)



from . models import SalesTracker

class SalesTrackerAdmin(admin.ModelAdmin):
    list_display = ('date', 'lead_id', 'status', 'remarks', 'phone', 'last_updated')
    search_fields = ['date', 'lead_id', 'status', 'remarks', 'phone']
    list_filter = ('date', 'status', 'last_updated')

admin.site.register(SalesTracker, SalesTrackerAdmin)


from . models import Lead

class LeadAdmin(admin.ModelAdmin):
    list_display = ('lead_id', 'Available_date', 'Name', 'Mobile', 'Project_Name', 'Project_Details', 'Project_Type', 'Lead_Status', 'Lead_source')
    search_fields = ['lead_id', 'Name', 'Mobile', 'Project_Name', 'Project_Details', 'Project_Type', 'Lead_Status', 'Lead_source']
    list_filter = ('Available_date', 'Lead_Status', 'Lead_source')


admin.site.register(Lead, LeadAdmin)


from . models import Attendence

class AttendenceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'checkin_time', 'checkout_time', 'status', 'created_date')
    search_fields = ['id', 'name', 'checkin_time', 'checkout_time', 'status', 'created_date']
    list_filter = ('status', 'created_date')

admin.site.register(Attendence, AttendenceAdmin)


from .models import Notice

class NoticeAdmin(admin.ModelAdmin):
    list_display = ('id', 'notice_type', 'description', 'notice_date', 'notice_by')
    search_fields = ['id', 'notice_type', 'description', 'notice_date', 'notice_by']
    list_filter = ('notice_date', 'notice_by')

admin.site.register(Notice, NoticeAdmin)


from . models import Payroll

class PayrollAdmin(admin.ModelAdmin):
    list_display = ('employee_id', 'employee_name', 'net_salary', 'basic_salary')
    search_fields = ['employee_id', 'employee_name', 'net_salary', 'basic_salary']
    list_filter = ('employee_name','employee_id')

admin.site.register(Payroll, PayrollAdmin)


from .models import Performance

class PerformanceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'performance')
    search_fields = ['id', 'name', 'performance']
    list_filter = ('name', 'performance')

admin.site.register(Performance, PerformanceAdmin)


from .models import UserManagement

class UserManagementAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email',  'password')
    search_fields = ['id', 'name', 'email',  'password']
    list_filter = ('name', 'email')

admin.site.register(UserManagement, UserManagementAdmin)


from .models import Role

class RoleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(Role, RoleAdmin)