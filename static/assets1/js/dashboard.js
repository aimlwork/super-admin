// Sales Graph
const salesGraph = document.getElementById('sales-graph');
const salesGraphCtx = salesGraph.getContext('2d');
new Chart(salesGraphCtx, {
  type: 'line',
  data: {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
    datasets: [{
      label: 'Sales',
      data: [100, 200, 300, 400, 500],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgba(255, 99, 132, 1)',
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  }
});

// Lead Sources Pie Chart
const leadSourcesPie = document.getElementById('lead-sources-pie');
const leadSourcesPieCtx = leadSourcesPie.getContext('2d');
new Chart(leadSourcesPieCtx, {
  type: 'pie',
  data: {
    labels: ['Facebook', 'Google', 'Email', 'Referral'],
    datasets: [{
      label: 'Lead Sources',
      data: [30, 20, 15, 35],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)'
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)'
      ],
      borderWidth: 1
    }]
  },
  options: {
    responsive: true
  }
});